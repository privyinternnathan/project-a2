alert('Hello world!');

console.log('Hello world!');
console.error('This is an error');

// let, const, var

//variables
let age = 17;
age = 25;
console.log(age);

//primitive data types
const name = 'Jamal';
const rating = 4.6;
const isCool = true;
const x = null;
const y = undefined;
let z;

console.log(typeof x);

//Concatenation
console.log('My name is ' + name + ' and my age is ' + age);
const introduction = `My name is ${name} and my age is ${age}`;
console.log(introduction);

//String methods
//cut string & change to uppercase
const hello = 'Hello world!';
console.log(hello.substring(0,5).toUpperCase());

//split strings
const cars = 'Volvo, Honda, Mercedes';
console.log(cars.split(', '));

//Arrays

const fruits = ['oranges', 'pineapples', 'grapes'];
//manipulate array values
fruits[2] = 'apples';
fruits.push('durians');
fruits.unshift('strawberries');
fruits.pop();

//check if it's an array
console.log(Array.isArray('hello'));
//get index of array
console.log(fruits.indexOf('oranges'));

console.log(fruits);

//Object Literals
const profile = {
    firstName: 'Johnny',
    lastName: 'Bravo',
    age: 51,
    hobbies: ['gym', 'guitar', 'programming'],
    address: {
        street: '50 St Ave',
        City: 'San Diego',
        State: 'CA'
    }
}

const { firstName, lastName, address: { city}} = person;
person.email = 'johnnydoe@gmail.com';


//Parallel Array of Objects
const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with counsellor',
        isCompleted: false
    },
    {
        id: 3,
        text: 'Go gym',
        isCompleted: true
    }
];

console.log(todos[1]); //takes first array data

//Convert to JSON
const todoJSON = JSON.stringify(todos);
console.log(todoJSON);

//For loops (same like Java)
for (let i = 0; i < 5; i++) {
    console.log('For loop no' + i);
}

//While loops
let i = 0;
while (i < 10) {
    console.log(`While loop no: ${i}`);
};

//Filter out variales
for (let todo of todos) {
    console.log(todo.id);
};

//forEach, map, filter
const todoCompleted = todos.filter(function(todo) {
    return todo.isCompleted === true;
}).map(function(todo) {
    return todo.text;
})

console.log(todoCompleted);

//Conditionals
//if statements
const a = 5;

if (a === 10) {
    console.log('A is ' + a);
} else if (x > 10) {
    console.log('A is more than' + a);
} else {
    console.log('A is less than' + a);
}

const k = 6;
const n = 11;

//combined with && (AND) & || (OR)
if (k > 5 && y > 10) {
    console.log('X is more tha 5 or y is more than 10');
}

const b = 10;
const color = b > 10 ? 'red' : 'blue';

switch(color) {
    case 'red':
        console.log('color is red');
        break;
    case 'blue':
        console.log('Color is blue');
    default:
        console.log('color is NOT red or blue');
        break;
}


//Functions
function addNums(num1 = 1, num2 = 1) {
    return num1 + num2;
}

console.log(addNums(5, 4));


//OOP (object oriented programming) in JS

//Constructors
function Person(firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);

    //Functions for getter/setter
    this.getBirthYear = function() {
        return this.dob.getFullYear();
    }

    this.getFullName = function() {
        return `${this.firstName} ${this.lastName}`;
    }
}

//Instantiation (4 Pillars)
const person1 = new Person('Nate', 'Higgers', '1-1-1970');
const person2 = new Person('John', 'Dover', '8-11-1998');

console.log(person1);

//Prototypes (Similar concept to inheritance but in JS)
Person.prototype.getBirthYear = function() {
    return this.dob.getFullYear();
}

Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`;
}

// Built in constructors
const names = new String('Kevin');
console.log(typeof names); // Shows 'Object'
const num = new Number(5);
console.log(typeof num); // Shows 'Object'


// ES6 Classes
class Person {
    constructor(firstName, lastName, dob) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.dob = new Date(dob);
    }
  
    getBirthYear() {
      return this.dob.getFullYear();
    }
  
    getFullName() {
      return `${this.firstName} ${this.lastName}`
    }
  }



//DOM Selection
console.log(window);
alert(1);

//Element Selectors

// Single Element
console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));

// Multiple Element
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item));

//Manipulating the DOM (UI)
const ul = document.querySelector('.items');

// ul.remove();
// ul.lastElementChild.remove();
ul.firstElementChild.textContent = 'Hello';
ul.children[1].innerText = 'Brad';
ul.lastElementChild.innerHTML = '<h1>Hello</h1>'; //Inner HTML

const btn = document.querySelector('.btn');
// btn.style.background = 'red';


//Events
//mouse, keyboard, etc.

// Mouse Event
//button properties
btn.addEventListener('click', e => {
    e.preventDefault();
    console.log(e.target.className);
    document.getElementById('my-form').style.background = '#ccc';
    document.querySelector('body').classList.add('bg-dark');
    ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
  }); //Enables change when mouse is hovered over an object

// Keyboard Event
const nameInputs = document.querySelector('#name');
nameInput.addEventListener('input', e => {
  document.querySelector('.container').append(nameInput.value);
});


// Form Script

// Put DOM elements into variables
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

// Listen for form submit
myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
  e.preventDefault();
  
  if(nameInput.value === '' || emailInput.value === '') {
    // alert('Please enter all fields');
    msg.classList.add('error');
    msg.innerHTML = 'Please enter all fields';

    // Remove error after 3 seconds
    setTimeout(() => msg.remove(), 3000);
  } else {
    // Create new list item with user
    const li = document.createElement('li');

    // Add text node with input values
    li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

    // Add HTML
    // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;

    // Append to ul
    userList.appendChild(li);

    // Clear fields
    nameInput.value = '';
    emailInput.value = '';
  }
}